<!---

[![Doc](https://img.shields.io/badge/doc-master-blue.svg)](http://track-analyzer.pages.pasteur.fr/track-analyzer)
[![Open Source License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://opensource.org/licenses/GPL-3.0)
[![PyPI](https://img.shields.io/pypi/v/track-analyzer)](https://pypi.org/project/track-analyzer/)
![Conda](https://img.shields.io/conda/pn/bioconda/track_analyzer)

-->


# Manipylator
A Python-driven manipulator controller

**Manipylator** is Python-based controller used to run a motorized manipulator through a user-friendly graphical interface. 
This package offers several mechanical assays to physically manipulate a sample.  
In particular, **Manipylator** provides in real time controller to exert controlled forces through a cantilever-based force sensor and actuator. 

**Manipylator** currently supports PI ([Physik Instrumente](https://www.physikinstrumente.com/en/)) motorized stages to drive the manipulator.
