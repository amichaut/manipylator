#!/usr/bin/env python

import time
import matplotlib.pyplot as plt
from simple_pid import PID


class WaterBoiler:
    """
    Simple simulation of a water boiler which can heat up water
    and where the heat dissipates slowly over time
    """

    def __init__(self):
        self.water_temp = 20

    def update(self, boiler_power, dt):
        if boiler_power > 0:
            # Boiler can only produce heat, not cold
            self.water_temp += 1 * boiler_power * dt

        # Some heat dissipation
        self.water_temp -= 0.02 * dt
        return self.water_temp

color_list = plt.rcParams['axes.prop_cycle'].by_key()['color']

boiler = WaterBoiler()
water_temp = boiler.water_temp

pid = PID(5, 0.01, 0.1, setpoint=water_temp)
pid.output_limits = (0, 100)

start_time = time.time()
last_time = start_time

#PID controller updating frequency
frequency = 50  # Hz
period = 1.0/frequency

# Keep track of values for plotting
setpoint, y, x = [], [], []

#fig,ax = plt.subplots()
#plt.ion()#

#fig.show()
#fig.canvas.draw()


while time.time() - start_time < 10:
    current_time = time.time()
    
    dt = current_time - last_time

    power = pid(water_temp)
    water_temp = boiler.update(power, dt)

    x += [current_time - start_time]
    y += [water_temp]
    setpoint += [pid.setpoint]

#    ax.scatter(current_time - start_time, water_temp, color=color_list[0])
#    ax.scatter(current_time - start_time, pid.setpoint, color=color_list[1])
#    fig.canvas.draw()

    if current_time - start_time > 1:
        pid.setpoint = 100

    last_time = current_time

    # sleep so PID isn't always computing
    while (time.time() - current_time) < period:
        time.sleep(0.001)  # precision here 

plt.plot(x, y, label='measured')
plt.plot(x, setpoint, label='target')
plt.xlabel('time')
plt.ylabel('temperature')
plt.legend()
plt.show()