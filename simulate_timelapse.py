import os
import os.path as osp
import time
import sys
import shutil
import argparse


def parse_args(args=None):
    """
    parse arguments for main()
    """

    parser = argparse.ArgumentParser(description="""
        Simulate a timelapse acquisition by copying a folder of tiff file to another one
        """)

    parser.add_argument('in_dir',
                        help='path of the input directory')

    parser.add_argument('out_dir',
                        help='path of the input directory')

    parser.add_argument('time_step',
                        help='time step in seconds')


    parsed_args = parser.parse_args(args)

    return parsed_args


def main(args=None):
    """

    """
    args = sys.argv[1:] if args is None else args
    parsed_args = parse_args(args)

    in_dir = osp.realpath(parsed_args.in_dir)
    out_dir = osp.realpath(parsed_args.out_dir)
    time_step = float(parsed_args.time_step)

    # generate list of tiff files  
    fn_list = []
    for fn in os.listdir(in_dir):
        if fn.endswith('.tif') or fn.endswith('.tiff'):
            if not osp.isdir(osp.join(in_dir,fn)):
                fn_list.append(fn)

    print('copying files from {} to {} \n'.format(in_dir,out_dir))


    start_time = time.time()

    for fn in sorted(fn_list):
        current_time = time.time()

        # copy file to location
        shutil.copyfile(osp.join(in_dir,fn), osp.join(out_dir,fn))
        sys.stdout.write("\033[K")  # go back to previous line
        print('copying {}'.format(fn), flush=True, end='\r')

        # wait time_step
        while (time.time() - current_time) < time_step:
            time.sleep(0.001)  # precision here 

if __name__ == '__main__':
    main()